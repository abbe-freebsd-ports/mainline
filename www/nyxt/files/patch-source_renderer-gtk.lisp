--- source/renderer-gtk.lisp.orig	2020-06-20 16:23:42 UTC
+++ source/renderer-gtk.lisp
@@ -67,7 +67,10 @@ See https://github.com/atlas-engineer/nyxt/issues/740"
           (gdk:gdk-set-program-class "nyxt")
           (finalize browser urls startup-timestamp))
         (unless *keep-alive*
-          (gtk:join-gtk-main)))
+          #-freebsd
+          (gtk:join-gtk-main)
+          #+freebsd
+          (gtk:gtk-main)))
       #+darwin
       (progn
         (setf gtk-running-p t)
