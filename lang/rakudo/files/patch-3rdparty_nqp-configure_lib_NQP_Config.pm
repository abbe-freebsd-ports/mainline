--- 3rdparty/nqp-configure/lib/NQP/Config.pm.orig
+++ 3rdparty/nqp-configure/lib/NQP/Config.pm
@@ -1095,76 +1095,7 @@
 sub git_checkout {
     my ( $self, $repo, $dir, $checkout ) = @_;
 
-    die "Unknown repository '$repo' in call to git_checkout"
-      unless $self->{repo_maps}{$repo};
-
-    my $config  = $self->config;
-    my $options = $self->{options};
-    my $pwd     = cwd();
-    my $pullurl = $self->repo_url( $repo, action => 'pull' );
-    my $pushurl = $self->repo_url( $repo, action => 'push' );
-
-    # Clone / fetch git reference repo
-    if ( $config->{git_cache_dir} ) {
-        my $ref_dir =
-          $self->reference_dir( $self->{repo_maps}{$repo}[1] );
-        if ( !-d $ref_dir ) {
-            my @ref_args = ( 'git', 'clone', '--bare' );
-            push @ref_args, "--depth=$options->{'git-depth'}"
-              if $options->{'git-depth'};
-            push @ref_args, $pullurl, $ref_dir;
-            $self->msg("Cloning reference from $pullurl\n");
-            system_or_die(@ref_args);
-        }
-        else {
-            chdir($ref_dir);
-            system_or_die( 'git', 'fetch' );
-            chdir($pwd);
-        }
-    }
-    # get an up-to-date repository
-    if ( !-d $dir ) {
-        my @args = ( 'git', 'clone' );
-        if ( $config->{git_cache_dir} ) {
-            my $ref_dir =
-              $self->reference_dir( $self->{repo_maps}{$repo}[1] );
-            die "Can't find $repo reference directory in $config->{git_cache_dir}"
-              unless $ref_dir;
-            push @args, "--reference=$ref_dir";
-        }
-        push @args, "--depth=$options->{'git-depth'}"
-          if $options->{'git-depth'};
-        push @args, $pullurl, $dir;
-        $self->msg("Cloning from $pullurl\n");
-        system_or_die(@args);
-        chdir($dir);
-
-        system( 'git', 'config', 'remote.origin.pushurl', $pushurl )
-          if defined $pushurl && $pushurl ne $pullurl;
-    }
-    else {
-        chdir($dir);
-        system_or_die( 'git', 'fetch' );
-
-        # pre-git 1.9/2.0 `--tags` did not fetch tags in addition to normal
-        # fetch https://stackoverflow.com/a/20608181/2410502 so do it separately
-        system_or_die( 'git', 'fetch', '--tags' );
-    }
-
-    if ($checkout) {
-        system_or_die( 'git', 'checkout', $checkout );
-        system_or_die( 'git', 'pull' )
-          if slurp('.git/HEAD') =~ /^ref:/;
-    }
-
-    my $git_describe;
-    if ( open( my $GIT, '-|', "git describe --tags" ) ) {
-        $git_describe = <$GIT>;
-        close($GIT);
-        chomp $git_describe;
-    }
-    chdir($pwd);
-    $git_describe;
+    'nqp-%%PORTVERSION%%';
 }
 
 sub _restore_ctx {
