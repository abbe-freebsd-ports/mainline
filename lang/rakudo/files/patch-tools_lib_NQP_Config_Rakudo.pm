--- tools/lib/NQP/Config/Rakudo.pm.orig
+++ tools/lib/NQP/Config/Rakudo.pm
@@ -673,7 +673,7 @@
     }
 
     my @cmd = (
-        $^X, 'Configure.pl', qq{--prefix=$prefix}, "--make-install",
+        $^X, 'Configure.pl', qq{--prefix=%%STAGEDIR%%$prefix}, "--make-install",
         "--git-protocol=$git_protocol",
     );
 
