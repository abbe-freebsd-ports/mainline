--- cargo-crates/libc-0.2.77/src/unix/bsd/freebsdlike/freebsd/mod.rs.orig	2020-10-29 10:32:25 UTC
+++ cargo-crates/libc-0.2.77/src/unix/bsd/freebsdlike/freebsd/mod.rs
@@ -129,6 +129,11 @@ s! {
         pub pve_fsid: u32,
         pub pve_path: *mut ::c_char,
     }
+
+    pub struct itimerspec {
+        pub it_interval: ::timespec,
+        pub it_value: ::timespec,
+    }
 }
 
 s_no_extra_traits! {
