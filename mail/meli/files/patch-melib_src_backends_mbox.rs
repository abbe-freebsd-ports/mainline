--- melib/src/backends/mbox.rs.orig	2020-10-29 10:24:27 UTC
+++ melib/src/backends/mbox.rs
@@ -65,6 +65,7 @@ fn get_rw_lock_blocking(f: &File) {
         l_len: 0, /* "Specifying 0 for l_len has the special meaning: lock all bytes starting at the location
                   specified by l_whence and l_start through to the end of file, no matter how large the file grows." */
         l_pid: 0, /* "By contrast with traditional record locks, the l_pid field of that structure must be set to zero when using the commands described below." */
+        l_sysid: 0, /* "By contrast with traditional record locks, the l_pid field of that structure must be set to zero when using the commands described below." */
     };
     let ptr: *mut libc::flock = &mut flock;
     let ret_val = unsafe { libc::fcntl(fd, F_OFD_SETLKW, ptr as *mut libc::c_void) };
