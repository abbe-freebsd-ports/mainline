--- src/libutil/util.cc.orig	2021-02-07 15:52:25 UTC
+++ src/libutil/util.cc
@@ -910,7 +910,7 @@ void killUser(uid_t uid)
 #else
             if (kill(-1, SIGKILL) == 0) break;
 #endif
-            if (errno == ESRCH) break; /* no more processes */
+            if (errno == ESRCH || errno == EPERM) break; /* no more processes */
             if (errno != EINTR)
                 throw SysError(format("cannot kill processes for uid '%1%'") % uid);
         }
