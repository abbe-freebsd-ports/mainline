--- src/hexchat_otr.c.orig	2017-02-05 20:03:01 UTC
+++ src/hexchat_otr.c
@@ -374,7 +374,9 @@ void printformat (IRC_CTX *ircctx, const
 
 	hexchat_set_context (ph, find_query_ctx);
 
+#pragma GCC diagnostic ignored "-Wformat-nonliteral"
 	if (g_vsnprintf (msg, sizeof(msg), formats[fnum].def, params) < 0)
+#pragma GCC diagnostic pop
 		g_snprintf (msg, sizeof(msg), "internal error parsing error string (BUG)");
 	va_end (params);
 	hexchat_printf (ph, "OTR: %s", s);
