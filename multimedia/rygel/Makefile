# Created by: Ashish SHUKLA <ashish@FreeBSD.org>
# $FreeBSD$

PORTNAME=	rygel
PORTVERSION=	0.31.3
CATEGORIES=	multimedia
MASTER_SITES=	GNOME

MAINTAINER=	ashish@FreeBSD.org
COMMENT=	Home media solution (UPnP AV MediaServer)

LICENSE=	LGPL21
LICENSE_FILE=	${WRKSRC}/COPYING

BUILD_DEPENDS=	vala:lang/vala \
		docbook-xsl>=0:textproc/docbook-xsl
LIB_DEPENDS=	libgssdp-1.0.so:net/gssdp \
		libgupnp-1.0.so:net/gupnp \
		libgupnp-av-1.0.so:net/gupnp-av \
		libgupnp-dlna-2.0.so:net/gupnp-dlna \
		libgstreamer-1.0.so:multimedia/gstreamer1 \
		libgstpbutils-1.0.so:multimedia/gstreamer1-plugins \
		libsoup-2.4.so:devel/libsoup \
		libgee-0.8.so:devel/libgee \
		libmediaart-2.0.so:multimedia/libmediaart

OPTIONS_DEFINE=	TRACKER
OPTIONS_SUB=	yes

TRACKER_DESC=	Enable Tracker plugin

USES=		gettext gmake libtool pathfix pkgconfig tar:xz sqlite:3

GNU_CONFIGURE=	yes
USE_GNOME=	glib20 libxml2 gtk30 libxslt
USE_LDCONFIG=	yes
INSTALLS_ICONS=	yes
INSTALL_TARGET=	install-strip

CONFIGURE_ARGS=	--enable-mpris-plugin \
		--enable-ruih-plugin \
		--enable-lms-plugin \
		--enable-playbin-plugin \
		--enable-media-export-plugin \
		--enable-gst-launch-plugin \
		--enable-example-plugins

TRACKER_CONFIGURE_ENABLE=	tracker-plugin
TRACKER_LIB_DEPENDS=		libtracker-sparql-1.0.so:sysutils/tracker

.include <bsd.port.mk>
