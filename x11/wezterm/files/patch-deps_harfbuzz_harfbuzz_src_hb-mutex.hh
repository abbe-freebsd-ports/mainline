--- deps/harfbuzz/harfbuzz/src/hb-mutex.hh.orig	2020-12-23 21:04:17 UTC
+++ deps/harfbuzz/harfbuzz/src/hb-mutex.hh
@@ -48,7 +48,7 @@
 /* Defined externally, i.e. in config.h; must have typedef'ed hb_mutex_impl_t as well. */
 
 
-#elif !defined(HB_NO_MT) && (defined(HAVE_PTHREAD) || defined(__APPLE__))
+#elif !defined(HB_NO_MT) && (defined(HAVE_PTHREAD) || defined(__APPLE__) || defined(__FreeBSD__))
 
 #include <pthread.h>
 typedef pthread_mutex_t hb_mutex_impl_t;
